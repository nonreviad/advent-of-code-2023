import System.IO

main :: IO ()
main = do
        handle <- openFile "input.txt" ReadMode
        contents <- hGetContents handle
        let coords = parseLines (lines contents)
        print (partOne coords)
        print (partTwo coords)
        hClose handle

data Coordinate = Coordinate {x :: Int , y :: Int } deriving (Show)

parseLine' :: Int -> Int -> String -> [Coordinate]
parseLine' xx yy s = case s of
    [] -> []
    '#':rest -> Coordinate {x = xx, y = yy}:parseLine' (xx+1) yy rest
    _:rest -> parseLine' (xx+1) yy rest

parseLine :: Int -> String -> [Coordinate]
parseLine = parseLine' 0

parseLines' :: Int -> [String] -> [Coordinate]
parseLines' line_idx lines = case lines of
    []      -> []
    foo:bar -> parseLine line_idx foo ++ parseLines' (line_idx + 1) bar

parseLines :: [String] -> [Coordinate]
parseLines = parseLines' 0

getMinX :: [Coordinate] -> Int
getMinX coords = case coords of
    Coordinate {x = foo}:rest -> case rest of
        [] -> foo
        _ -> min foo (getMinX rest)

getMaxX :: [Coordinate] -> Int
getMaxX coords = case coords of
    Coordinate {x = foo}:rest -> case rest of
        [] -> foo
        _ -> max foo (getMaxX rest)

getMinY :: [Coordinate] -> Int
getMinY coords = case coords of
    Coordinate {y = foo}:rest -> case rest of
        [] -> foo
        _ -> min foo (getMinY rest)

getMaxY :: [Coordinate] -> Int
getMaxY coords = case coords of
    Coordinate {y = foo}:rest -> case rest of
        [] -> foo
        _ -> max foo (getMaxY rest)

getXS :: [Coordinate] -> [Int]
getXS coords = case coords of
    [] -> []
    Coordinate {x = foo}:rest -> foo:getXS rest

getYS :: [Coordinate] -> [Int]
getYS coords = case coords of
    [] -> []
    Coordinate {y = foo}:rest -> foo:getYS rest

isIn :: (Eq a) => [a] -> a -> Bool
isIn v x = case v of
    [] -> False
    v:rest -> v == x || isIn rest x

isNotIn :: (Eq a) => [a] -> a -> Bool
isNotIn v x = not (isIn v x)

notFoundXs :: [Coordinate] -> [Int]
notFoundXs coords =
    let min_x = getMinX coords
        max_x = getMaxX coords
        xs = getXS coords
    in filter (isNotIn xs) [min_x..max_x]

notFoundYs :: [Coordinate] -> [Int]
notFoundYs coords =
    let min_y = getMinY coords
        max_y = getMaxY coords
        ys = getYS coords
    in filter (isNotIn ys) [min_y..max_y]

shiftCoordX :: Int -> (Coordinate, Coordinate) -> Int -> (Coordinate, Coordinate)
shiftCoordX shift_amount coord_tuple foo = case coord_tuple of
    (Coordinate {x = orig_x, y = orig_y}, Coordinate {x = shifted_x, y = shifted_y}) ->
        if orig_x > foo then (Coordinate {x = orig_x, y = orig_y}, Coordinate {x = shifted_x+shift_amount, y = shifted_y}) else coord_tuple

shiftAllXCoords :: Int -> (Coordinate, Coordinate) -> [Int] -> Coordinate
shiftAllXCoords shift_amount coord_tuple foos = case foos of
    [] -> case coord_tuple of
        (_, shifted) -> shifted
    (foo:rest) -> shiftAllXCoords shift_amount (shiftCoordX shift_amount coord_tuple foo) rest

shiftCoordY :: Int -> (Coordinate, Coordinate) -> Int -> (Coordinate, Coordinate)
shiftCoordY shift_amount coord_tuple foo = case coord_tuple of
    (Coordinate {x = orig_x, y = orig_y}, Coordinate {x = shifted_x, y = shifted_y}) ->
        if orig_y > foo then (Coordinate {x = orig_x, y = orig_y}, Coordinate {x = shifted_x, y = shifted_y+shift_amount}) else coord_tuple

shiftAllYCoord :: Int -> (Coordinate, Coordinate) -> [Int] -> Coordinate
shiftAllYCoord shift_amount coord_tuple foos = case foos of
    [] -> case coord_tuple of
        (_, shifted) -> shifted
    (foo:rest) -> shiftAllYCoord shift_amount (shiftCoordY shift_amount coord_tuple foo) rest

shiftCoords :: Int -> [Coordinate] -> [Coordinate]
shiftCoords shift_amount coords =
    let not_xs = notFoundXs coords
        not_ys = notFoundYs coords
        shifted_xs = map (\x -> shiftAllXCoords shift_amount (x, x) not_xs) coords
        shifted_ys = map (\x -> shiftAllYCoord shift_amount (x, x) not_ys) shifted_xs
    in shifted_ys

manhattanDistance :: (Coordinate, Coordinate) -> Int
manhattanDistance coord_tuple = case coord_tuple of
    (Coordinate {x = x1, y = y1}, Coordinate {x = x2, y = y2}) ->
        abs (x2-x1) + abs (y2-y1)

allPairs :: [a] -> [(a,a)]
allPairs l = case l of
    [] -> []
    [_] -> []
    x:rest -> map (x,) rest ++ allPairs rest

partOne :: [Coordinate] -> Int
partOne coords =
    let shifted_coords = shiftCoords 1 coords
        coord_pairs = allPairs shifted_coords
    in sum (map manhattanDistance coord_pairs)

partTwo :: [Coordinate] -> Int
partTwo coords =
    let shifted_coords = shiftCoords 999999 coords
        coord_pairs = allPairs shifted_coords
    in sum (map manhattanDistance coord_pairs)

