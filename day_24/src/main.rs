#![allow(non_snake_case)]
use nalgebra::{Matrix6, Vector6};

struct Hailstone {
    px: i64,
    py: i64,
    pz: i64,
    vx: i64,
    vy: i64,
    vz: i64,
}

fn intersect_2d(
    min_x: i64,
    max_x: i64,
    min_y: i64,
    max_y: i64,
    hailstone1: &Hailstone,
    hailstone2: &Hailstone,
) -> bool {
    /*
        vx1*t1 - vx2 * t2  = px2 - px1
        vy1*t1 - vy2 * t2  = py2 - py1
        ------------------------------
    vx1*vy1*t1 - vx2 * vy1 * t2 = (px2 - px1) * vy1
    vx1*vy1*t1 - vy2 * vx1 * t2 = (py2 - py1) * vx1

    t2 = ((px2 - px1) * vy1 - (py2 - py1) * vx1) / (-vx2 * vy1 + vy2 * vx1)
     */
    let vx1 = hailstone1.vx as f64;
    let vy1 = hailstone1.vy as f64;
    let px1 = hailstone1.px as f64;
    let py1 = hailstone1.py as f64;

    let vx2 = hailstone2.vx as f64;
    let vy2 = hailstone2.vy as f64;
    let px2 = hailstone2.px as f64;
    let py2 = hailstone2.py as f64;

    if (-vx2 * vy1 + vy2 * vx1) == 0f64 {
        // Parallel or colinear; need to treat this differently?
        return false;
    }

    let t2 = ((px2 - px1) * vy1 - (py2 - py1) * vx1) / (-vx2 * vy1 + vy2 * vx1);
    let t1 = (px2 - px1 + vx2 * t2) / vx1;
    if t2 < 0f64 {
        return false;
    }
    if t1 < 0f64 {
        return false;
    }

    let x = px1 + t1 * vx1;
    let y = py1 + t1 * vy1;

    let min_x = min_x as f64;
    let min_y = min_y as f64;
    let max_x = max_x as f64;
    let max_y = max_y as f64;

    if x < min_x || x > max_x || y < min_y || y > max_y {
        return false;
    }

    true
}

fn main() {
    let input_data = include_str!("../input.txt");
    let hailstones: Vec<Hailstone> = input_data
        .lines()
        .map(|line| {
            let (positions, speeds) = line.split_once(" @ ").expect("Line doesnt contain @");
            let (px, pypz) = positions.split_once(", ").unwrap();
            let px = px.trim();
            let px = px.parse::<i64>().unwrap();
            let (py, pz) = pypz.split_once(", ").unwrap();
            let py = py.trim();
            let py = py.parse::<i64>().unwrap();
            let pz = pz.trim();
            let pz = pz.parse::<i64>().unwrap();

            let (vx, vyvz) = speeds.split_once(", ").unwrap();
            let vx = vx.trim();
            let vx = vx.parse::<i64>().unwrap();
            let (vy, vz) = vyvz.split_once(", ").unwrap();
            let vy = vy.trim();
            let vy = vy.parse::<i64>().unwrap();
            let vz = vz.trim();
            let vz = vz.parse::<i64>().unwrap();
            Hailstone {
                px,
                py,
                pz,
                vx,
                vy,
                vz,
            }
        })
        .collect();
    // Part one
    let mut intersecting = 0;
    for i1 in 0..hailstones.len() - 1 {
        for i2 in i1 + 1..hailstones.len() {
            if intersect_2d(
                200000000000000,
                400000000000000,
                200000000000000,
                400000000000000,
                &hailstones[i1],
                &hailstones[i2],
            ) {
                intersecting += 1;
            }
        }
    }
    println!("{intersecting}");
    //
    // Part two
    // The hailstones are guaranteed to never collide.
    // These are true for all ti, i = 0..hailstones.len()
    // x + vx * t0 =  x0 + vx0 * t0
    // y + vy * t0 =  y0 + vy0 * t0
    // z + vz * t0 =  z0 + vz0 * t0
    //
    // We can remove ti's by computing them for each triplet of equations, and then equating them 2 by 2.
    // (x - x0) * (vy0 - vy) - (y - y0) * (vx0 - vx) = 0
    // (x - x0) * (vz0 - vz) - (z - z0) * (vx0 - vx) = 0
    // (y - y0) * (vz0 - vz) - (z - z0) * (vy0 - vy) = 0
    //
    // Expanding them, we have 'mixed' terms. This can be fixed by computing the same for two more i's, and subtracting them in pairs,
    // to end up with 6 eq'ns.
    //
    // x * (vy0 - vy1) + y * (vx1 - vx0) + z *      0      + vx * (-y0 + y1) + vy * ( x0 - x1) + vz *     0     = -vx0*y0 + vx1*y1 + vy0*x0 - vy1*x1
    // x * (vz0 - vz1) + y *      0      + z * (vx1 - vx0) + vx * (-z0 + z1) + vy *      0     + vz * (x0 - x1) = -vx0*z0 + vx1*z1 + vz0*x0 - vz1*x1
    // x *      0      + y * (vz0 - vz1) + z * (vy1 - vy0) + vx *      0     + vy * (-vy + z1) + vz * (y0 - y1) = -vy0*z0 + vy1*z1 + vz0*y0 - vz1*y1
    // x * (vy0 - vy2) + y * (vx2 - vx0) + z *      0      + vx * (-y0 + y2) + vy * ( x0 - x2) + vz *     0     = -vx0*y0 + vx2*y2 + vy0*x0 - vy2*x2
    // x * (vz0 - vz2) + y *      0      + z * (vx2 - vx0) + vx * (-z0 + z2) + vy *      0     + vz * (x0 - x2) = -vx0*z0 + vx2*z2 + vz0*x0 - vz2*x2
    // x *      0      + y * (vz0 - vz2) + z * (vy2 - vy0) + vx *      0     + vy * (-vy + z2) + vz * (y0 - y2) = -vy0*z0 + vy2*z2 + vz0*y0 - vz2*y2
    // Or, in matrix form, AX = Y, where X is a column vector with (x, y, z, vx, vy, vz)
    // A^-1 * Y = X_hat, the solution. This assumes the hailstones' parameters are linearly independent s.t. A is invertible.
    let x0 = hailstones[0].px;
    let y0 = hailstones[0].py;
    let z0 = hailstones[0].pz;
    let vx0 = hailstones[0].vx;
    let vy0 = hailstones[0].vy;
    let vz0 = hailstones[0].vz;

    let x1 = hailstones[1].px;
    let y1 = hailstones[1].py;
    let z1 = hailstones[1].pz;
    let vx1 = hailstones[1].vx;
    let vy1 = hailstones[1].vy;
    let vz1 = hailstones[1].vz;

    let x2 = hailstones[2].px;
    let y2 = hailstones[2].py;
    let z2 = hailstones[2].pz;
    let vx2 = hailstones[2].vx;
    let vy2 = hailstones[2].vy;
    let vz2 = hailstones[2].vz;
    let A = Matrix6::new(
        vy0 - vy1,
        vx1 - vx0,
        0,
        -y0 + y1,
        x0 - x1,
        0,
        vz0 - vz1,
        0,
        vx1 - vx0,
        -z0 + z1,
        0,
        x0 - x1,
        0,
        vz0 - vz1,
        vy1 - vy0,
        0,
        -z0 + z1,
        y0 - y1,
        vy0 - vy2,
        vx2 - vx0,
        0,
        -y0 + y2,
        x0 - x2,
        0,
        vz0 - vz2,
        0,
        vx2 - vx0,
        -z0 + z2,
        0,
        x0 - x2,
        0,
        vz0 - vz2,
        vy2 - vy0,
        0,
        -z0 + z2,
        y0 - y2,
    );
    let y = Vector6::new(
        -vx0 * y0 + vx1 * y1 + vy0 * x0 - vy1 * x1,
        -vx0 * z0 + vx1 * z1 + vz0 * x0 - vz1 * x1,
        -vy0 * z0 + vy1 * z1 + vz0 * y0 - vz1 * y1,
        -vx0 * y0 + vx2 * y2 + vy0 * x0 - vy2 * x2,
        -vx0 * z0 + vx2 * z2 + vz0 * x0 - vz2 * x2,
        -vy0 * z0 + vy2 * z2 + vz0 * y0 - vz2 * y2,
    );
    let A: Matrix6<f64> = A.map(|elem| elem as f64);
    let A_inverse = A.try_inverse().unwrap();
    let y: Vector6<f64> = y.map(|elem| elem as f64);
    let x_hat: Vector6<f64> = A_inverse * y;
    let sum = (x_hat[0] + x_hat[1] + x_hat[2]).round();
    println!("{sum}");
}
