(require '[clojure.string :as str])
(require '[clojure.java.io :as io])
(def input-file "../input.txt")

(defn  get-numbers [line]
  (mapv (fn [str] (Integer/parseInt str)) (str/split line #" ")))

(defn all-eq [val arr] (every? (fn [other_val] (= val other_val)) arr))

(defn all-same [arr]
  (if (<= (count arr) 1)
    true
    (all-eq (first arr) (rest arr))))

(defn next-step [arr] 
  (map 
   (fn [foo bar] (- foo bar))
   (rest arr) (take (- (count arr) 1) arr))
)

(defn next-number [arr]
  (if (all-same arr)
    (first arr)
    (let [last_number (last arr)]
      (+ last_number (next-number (next-step arr))))
  )
)

(next-number [0 3 6 9 12 15])

(with-open [rdr (io/reader input-file)]
  (println (reduce + (mapv next-number (mapv get-numbers (line-seq rdr)))))
  )

