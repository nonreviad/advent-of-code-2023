defmodule PartTwo do
  def all_same(l) do
    if l |> Enum.count() <= 1 do
      true
    else
      [h | tail] = l
      tail |> Enum.all?(fn (x) -> x == h end)
    end
  end

  def differences([]) do
    []
  end
  def differences([_h | []]) do
    []
  end
  def differences([foo | [bar | tail]]) do
    [bar - foo] ++ (differences([bar] ++ tail))
  end

  def prev_number(l) do
    [h | _tail] = l
    if all_same(l) do
        h
    else
      f = prev_number(differences(l))
      h - f
    end
  end

  def extract_ints(line) do
    line |> String.split(" ") |> Enum.map(fn (s) -> elem(Integer.parse(s), 0) end)
  end

  def run(input_file) do
    File.stream!(input_file) |> Enum.map(&extract_ints(&1)) |> Enum.map(&prev_number(&1)) |> Enum.sum()
  end
end

result = PartTwo.run("../input.txt")

IO.puts(to_string(result))
