#!/usr/bin/env python3
import math
FLIP_FLOP = 0
CONJUNCTION = 1
BROADCASTER = 2
END = 3

class Module():
    def __init__(self, str_data: str):
        splits = str_data.split(" -> ")
        self.recurrence = -1
        self.all_inputs = []
        if len(splits) == 1:
            self.deps = []
            self.kind = END
            self.name = str_data
        else:
            name, deps = splits
            self.deps = deps.split(", ")
            if name == "broadcaster":
                self.kind = BROADCASTER
                self.name = "broadcaster"
                self.recurrence = 1
            elif name.startswith("%"):
                self.kind = FLIP_FLOP
                self.name = name[1:]
                self.state = "off"
            else:
                self.kind = CONJUNCTION
                self.name = name[1:]
                self.memory = {}

    def reset(self):
        if self.kind == FLIP_FLOP:
            self.state = "off"
        elif self.kind == CONJUNCTION:
            for key in self.memory:
                self.memory[key] = "low"

    def recv_signal(self, from_input, signal):
        if self.kind == END:
            return []
        if self.kind == BROADCASTER:
            ret_signals = []
            for dep in self.deps:
                ret_signals.append((dep, self.name, signal))
            return ret_signals
        if self.kind == FLIP_FLOP:
            if signal == "high":
                return []
            new_signal = "low"
            if self.state == "off":
                self.state = "on"
                new_signal = "high"
            else:
                self.state = "off"
            ret_signals = []
            for dep in self.deps:
                ret_signals.append((dep, self.name, new_signal))
            return ret_signals
        if self.kind == CONJUNCTION:
            self.memory[from_input] = signal
            ret_signals = []
            if "low" in self.memory.values():
                for dep in self.deps:
                    ret_signals.append((dep, self.name, "high"))
            else:
                for dep in self.deps:
                    ret_signals.append((dep, self.name, "low"))
            return ret_signals

def push_button(modules_by_name, times):
    highs = 0
    lows = 0
    for _ in range(times):
        lows = lows + 1
        queue = modules_by_name["broadcaster"].recv_signal("button", "low")
        while len(queue) > 0:
            (dep, from_input, signal) = queue[0]
            if signal == "low":
                lows = lows + 1
            else:
                highs = highs + 1
            queue = queue[1:]
            queue = queue + modules_by_name[dep].recv_signal(from_input, signal)
    return highs * lows

def compute_recurrences(modules_by_name):
    times = 0
    recurrence_times = {}
    # I depend on rx having a single conjunction dep.
    deps_to_check = modules_by_name[modules_by_name['rx'].all_inputs[0]].all_inputs
    while True:
        times = times + 1
        queue = modules_by_name["broadcaster"].recv_signal("button", "low")
        while len(queue) > 0:
            (dep, from_input, signal) = queue[0]
            if dep in deps_to_check:
                if signal == "low" and dep not in recurrence_times:
                    recurrence_times[dep] = times
            queue = queue[1:]
            new_inputs = modules_by_name[dep].recv_signal(from_input, signal)
            queue = queue + new_inputs
        all_found = True
        for i in deps_to_check:
            if not i in recurrence_times:
                all_found = False
        if all_found:
            return math.lcm(*recurrence_times.values())
if __name__ == "__main__":
    modules = []
    modules_by_name = {}
    with open("input.txt", "r") as f:
        for line in f:
            m = Module(line.strip())
            modules_by_name[m.name] = m
            modules.append(m)
    conjunctions = set()
    end_module = Module("rx")
    modules.append(end_module)
    modules_by_name[end_module.name] = end_module
    for module in modules:
        if module.kind == CONJUNCTION:
            conjunctions.add(module.name)
    for module in modules:
        for dep in module.deps:
            if dep in conjunctions:
                modules_by_name[dep].memory[module.name] = "low"
            modules_by_name[dep].all_inputs.append(module.name)
    val = push_button(modules_by_name, 1000)
    print(str(val))
    for module in modules:
        module.reset()
    recurrence_time = compute_recurrences(modules_by_name)
    print(str(recurrence_time))
    