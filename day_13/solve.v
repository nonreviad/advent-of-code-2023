import v.util

struct Coordinate {
	x i32
	y i32
}

struct Pattern {
	rocks []Coordinate
	min_x i32
	min_y i32
	max_x i32
	max_y i32 
}

fn (p Pattern) vertical_symmetry(ignore i32) i32 {
	mut coords := map[i32][]i32 {}
	for coord in p.rocks {
		if coord.x in coords {
			coords[coord.x] << coord.y
		} else {
			coords[coord.x] = [coord.y]
		}
	}
	for x in p.min_x..p.max_x {
		mut dx := 0
		mut same := true
		for  {
			if (x - dx) < p.min_x || (x + dx + 1) > p.max_x || !same {
				break
			}
			x1 := i32(x - dx)
			x2 := i32(x + dx + 1)
			for y in coords[x1] {
				if !(y in coords[x2]) {
					same = false
					break
				}
			}
			for y in coords[x2] {
				if !(y in coords[x1]) {
					same = false
					break
				}
			}
			dx += 1
		}
		if same && ((x + 1) != ignore) {
			return x + 1
		}
	}
	return -1
}
fn (p Pattern) horizontal_symmetry(ignore i32) i32 {
	mut coords := map[i32][]i32 {}
	for coord in p.rocks {
		if coord.y in coords {
			coords[coord.y] << coord.x
		} else {
			coords[coord.y] = [coord.x]
		}
	}
	for y in p.min_y..p.max_y {
		mut dy := 0
		mut same := true
		for  {
			if (y - dy) < p.min_y || (y + dy + 1) > p.max_y || !same {
				break
			}
			y1 := i32(y - dy)
			y2 := i32(y + dy + 1)
			for x in coords[y1] {
				if !(x in coords[y2]) {
					same = false
					break
				}
			}
			for x in coords[y2] {
				if !(x in coords[y1]) {
					same = false
					break
				}
			}
			dy += 1
		}
		if same && ((y + 1) * 100) != ignore {
			return (y + 1) * 100
		}
	}
	return -1
}

fn (p Pattern) symmetry(ignore i32) i32 {
	sym1 := p.vertical_symmetry(ignore)
	if sym1 > 0 {
		return sym1
	}
	sym2 := p.horizontal_symmetry(ignore)
	if sym2 > 0 {
		return sym2
	}
	return -1
}
fn read_pattern(data string) Pattern {
	mut rocks := []Coordinate {}
	mut min_x := i32( 10000000000)
	mut min_y := i32( 10000000000)
	mut max_x := i32(-10000000000)
	mut max_y := i32(-10000000000)
	for y, line in data.split('\n') {
		for x, ch in line {
			if ch.ascii_str() == '#' {
				rocks << Coordinate { 
					x: i32(x)
					y: i32(y)
				}
				if x < min_x {
					min_x = i32(x)
				}
				if x > max_x {
					max_x = i32(x)
				}
				if y < min_y {
					min_y = i32(y)
				}
				if y > max_y {
					max_y = i32(y)
				}
			}
		}
	}
	return Pattern {
		rocks: rocks
		min_x: min_x
		max_x: max_x
		min_y: min_y
		max_y: max_y
	}
}

fn (p Pattern) clone_with(x i32, y i32) Pattern {
	mut new_rocks := []Coordinate{}
	new_rocks << Coordinate {x:x y:y}
	for rock in p.rocks {
		new_rocks << rock
	}
	return Pattern {
		rocks: new_rocks
		min_x: p.min_x
		max_x: p.max_x
		max_y: p.max_y
		min_y: p.min_y
	}
}

fn (p Pattern) clone_without(x i32, y i32) Pattern {
	mut new_rocks := []Coordinate{}
	for rock in p.rocks {
		if rock.x != x || rock.y != y {
			new_rocks << rock
		}
	}
	return Pattern {
		rocks: new_rocks
		min_x: p.min_x
		max_x: p.max_x
		max_y: p.max_y
		min_y: p.min_y
	}
}

fn (p Pattern) new_symmetry() i32 {
	initial_symmetry := p.symmetry(-1)
	mut x_coords :=  map[i32][]i32 {}
	for coord in p.rocks {
		if coord.x in x_coords {
			x_coords[coord.x] << coord.y
		} else {
			x_coords[coord.x] = [coord.y]
		}
	}
	for x in p.min_x..(p.max_x+1) {
		for y in p.min_y..(p.max_y+1) {
			if x in x_coords && y in x_coords[x] {
				new_p := p.clone_without(x, y)
				ns := new_p.symmetry(initial_symmetry)
				if ns != -1 && ns != initial_symmetry {
					return ns
				}
			} else {
				new_p := p.clone_with(x, y)
				ns := new_p.symmetry(initial_symmetry)
				if ns != -1 && ns != initial_symmetry {
					return ns
				}
			}
		}
	}
	return -1
}

fn part_one(filename string) {
	data := util.read_file(filename) or {panic (err)}
	mut sum := 0
	for line in data.split("\n\n") {
		pattern := read_pattern(line)
		sum += pattern.symmetry(-1)
	}
	println("$sum")
}

fn part_two(filename string) {
	data := util.read_file(filename) or {panic (err)}
	mut sum := 0
	for line in data.split("\n\n") {
		pattern := read_pattern(line)
		ns := pattern.new_symmetry()
		sum += ns
	}
	println("$sum")
}

fn main() {
	part_one("input.txt")
	part_two("input.txt")
}