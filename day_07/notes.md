~~Unfortunately, Whiley doesn't have functionality for reading from a file (I failed to double check this before today). Also, I couldn't make it build to JS for whatever reason, so I had to do a 'mapreduce' approach.~~

~~I calculate the 'score' of a hand in Whiley for each line, collect the results, then sort them by it in Typescript. This could be done in parallel, but :shrug:.~~

~~As a result, I chose to couple Whiley with Typescript on this day.~~

Never mind, I managed to make it work. It's super slow though.