type Position = {
  x: number,
  y: number,
}
type InfinitePosition = {
  x: number,
  y: number,
  global_x: number,
  global_y: number,
}
function hashyMcHashFace<T>(f: T): string;

function hashyMcHashFace(pos: Position): string {
  return `${pos.x},${pos.y}`;
}
function hashyMcHashFace(pos: InfinitePosition): string {
  return `${pos.x},${pos.y},${pos.global_x},${pos.global_y}`;
}
class AbstractSet<T> implements Iterable<T> {
  private map: Map<string, T>;

  constructor() {
    this.map = new Map();
  }

  private key(pos: T): string {
    return hashyMcHashFace(pos); // Create a unique string based on x and y
  }

  add(pos: T): void {
    this.map.set(this.key(pos), pos);
  }

  has(pos: T): boolean {
    return this.map.has(this.key(pos));
  }

  delete(pos: T): boolean {
    return this.map.delete(this.key(pos));
  }

  get size(): number {
    return this.map.size;
  }

  // Implement Iterable interface
  [Symbol.iterator](): Iterator<T> {
    return this.map.values();
  }
}


function regular_bfs(steps: number, start_point: Position, garden: AbstractSet<Position> ): AbstractSet<Position> {
  let visited: AbstractSet<Position> = new AbstractSet()
  visited.add(start_point)
  for (let i = 1; i <= steps; ++i) {
      let new_visited: AbstractSet<Position> = new AbstractSet()
      for (let old_pos of visited) {
          let {x, y} = old_pos
          let candidates: Position[] = [{x, y: y-1}, {x, y: y+1}, {x: x-1, y}, {x: x+1, y}];
          for (let candidate of candidates) {
              if (garden.has(candidate)) {
                  new_visited.add({x: candidate.x, y: candidate.y});
              }
          }
      }
      visited = new_visited
  }
  return visited
}

function part_one(start_point: Position, garden: AbstractSet<Position>, steps: number) {
  const visited = regular_bfs(steps, start_point, garden)
  console.log(visited.size)
}

function infinite_bfs(steps: number, start_point: Position, garden: AbstractSet<Position>, width: number, height: number): AbstractSet<InfinitePosition> {
  let visited: AbstractSet<InfinitePosition> = new AbstractSet()
  visited.add({x: start_point.x, y:start_point.y, global_x: 0, global_y: 0})
  for (let i = 1; i <= steps; ++i) {
      let new_visited: AbstractSet<InfinitePosition> = new AbstractSet()
      for (let old_pos of visited) {
          let {x, y, global_x, global_y} = old_pos
          let candidates: InfinitePosition[] = [{x, y: y-1, global_x, global_y}, {x, y: y+1, global_x, global_y}, {x: x-1, y, global_x, global_y}, {x: x+1, y, global_x, global_y}];
          for (let candidate of candidates) {
              if (candidate.x < 0) {
                candidate.x += width;
                candidate.global_x -= 1
              }
              if (candidate.y < 0) {
                candidate.y += height;
                candidate.global_y -= 1
              }
              if (candidate.x == width) {
                candidate.x -= width
                candidate.global_x += 1
              }
              if (candidate.y == height) {
                candidate.y -= height
                candidate.global_y += 1
              }
              if (garden.has({x: candidate.x, y: candidate.y})) {
                  new_visited.add({x: candidate.x, y: candidate.y, global_x: candidate.global_x, global_y: candidate.global_y});
              }
          }
      }
      visited = new_visited
  }
  return visited
}

function part_two(start_point: Position, garden: AbstractSet<Position>, width: number, height: number) {
  // H/T https://github.com/derailed-dash/Advent-of-Code/blob/master/src/AoC_2023/Dazbo's_Advent_of_Code_2023.ipynb
  // I could not solve this myself, so I used this as a resource to understand the solution. Upon reading this, I do not regret it. This problem is dumb.
  const cnt1 = infinite_bfs(65, start_point, garden, width, height).size
  const cnt2 = infinite_bfs(196, start_point, garden, width, height).size
  const cnt3 = infinite_bfs(327, start_point, garden, width, height).size
  const c = cnt1
  const a = (cnt3 - 2*cnt2 + cnt1) / 2
  const b = cnt2 - a - c
  const x = (26501365 - 65) / 131
  console.log(a * x * x + b * x + c)
}

let input_str: string = await Bun.file("./input.txt").text();
let lines: string[] = input_str.split("\n") 
let width: number = lines[0].length
let height: number = lines.length
let garden: AbstractSet<Position> = new AbstractSet();
let start_point: Position = {x: 0, y: 0}
for (let [y, line] of input_str.split("\n").entries()) {
  for (let x = 0; x < line.length; ++x) {
    let tile = line.charAt(x);
    if (tile == ".") {
      garden.add({x, y});
    } else if (tile == "S") {
      start_point = {x, y};
      garden.add({x, y})
    }
  }
}

part_one(start_point, garden, 64)
part_two(start_point, garden, width, height)

// console.log(visited.size)
