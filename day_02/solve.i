f = open("input.txt", "r")
sum_of_ids=0
sum_of_powers=0
for (game_id = 1; game_id <= 100; game_id++) {
    game = rdline(f);
    game_id_rounds = strtok(game, ":")
    game_id_ = game_id_rounds(1)
    rounds = game_id_rounds(2)
    red = 0;
    green = 0;
    blue = 0;
    do {
        round_and_rest = strtok(rounds, ";")
        curr_round = round_and_rest(1)
        rounds = round_and_rest(2)
        curr_round = strtrim(curr_round)
        do {
            curr_color_and_rest = strtok(curr_round, ",")
            curr_color = curr_color_and_rest(1)
            curr_round = curr_color_and_rest(2)

            curr_color = strtrim(curr_color)
            col_and_number = strtok(curr_color, " ")
            n = col_and_number(1)
            col = col_and_number(2)
            number = 0
            x = sread(n, format="%d", number);
            if (strmatch(col, "red") == 1) {
                red = max(red, number);
            }
            if (strmatch(col, "green") == 1) {
                green = max(green, number);
            }
            if (strmatch(col, "blue") == 1) {
                blue = max(blue, number);
            }
        } while(curr_round);
    } while(rounds);
    if (red <= 12 && green <= 13 && blue <= 14) {
        sum_of_ids += game_id;
    } else {
        print(game_id, red, green,blue)
    }
    p = red * green * blue;
    sum_of_powers += p;
}

print(sum_of_ids)
print(sum_of_powers)