import 'dart:io';
typedef Coordinate = ({int x, int y});

enum BeamType {
  Left,
  Right,
  Up,
  Down
}

enum MirrorType {
  LeanRight, // /
  LeanLeft,  // \
}

enum SplitterType {
  Vertical,   // |
  Horizontal, // -
}

typedef Mirror = ({Coordinate position, MirrorType mirror_type});
typedef Splitter = ({Coordinate position, SplitterType splitter_type});
typedef Beam = ({Coordinate position, BeamType beam_type});

// Convention -> x goes from 0 to width - 1, y from 0 to height - 1
class Board {
  int width = 0;
  int height = 0;
  Map<Coordinate, Splitter> splitters = new Map();
  Map<Coordinate, Mirror> mirrors = new Map();

  void addMirror(MirrorType mirror_type, Coordinate at_pos) {
    if (at_pos.x >= this.width) {
      this.width = at_pos.x + 1;
    }
    if (at_pos.y >= this.height) {
      this.height = at_pos.y + 1;
    }
    this.mirrors[at_pos] = (position: at_pos, mirror_type: mirror_type);
  }
  void addSplitter(SplitterType splitter_type, Coordinate at_pos) {
    if (at_pos.x >= this.width) {
      this.width = at_pos.x + 1;
    }
    if (at_pos.y >= this.height) {
      this.height = at_pos.y + 1;
    }
    this.splitters[at_pos] = (position: at_pos, splitter_type: splitter_type);
  }
  bool outOfBounds(Coordinate at_pos) {
    if (at_pos.x < 0 || at_pos.y < 0 || at_pos.x >= this.width || at_pos.y >= this.height) {
      return true;
    }
    return false;
  }
}

Beam advance(Beam beam) {
  switch (beam.beam_type) {
    case BeamType.Up:
      return (position: (x: beam.position.x, y: beam.position.y - 1), beam_type: beam.beam_type);
    case BeamType.Down:
      return (position: (x: beam.position.x, y: beam.position.y + 1), beam_type: beam.beam_type);
    case BeamType.Left:
      return (position: (x: beam.position.x - 1, y: beam.position.y), beam_type: beam.beam_type);
    case BeamType.Right:
      return (position: (x: beam.position.x + 1, y: beam.position.y), beam_type: beam.beam_type);
  }
}

Beam reflect(MirrorType mirror_type, Beam beam) {
  switch(mirror_type) {
    //  /
    case MirrorType.LeanRight:
      switch (beam.beam_type) {
        case BeamType.Left:
          return (position: (x: beam.position.x, y: beam.position.y + 1), beam_type: BeamType.Down);
        case BeamType.Right:
          return (position: (x: beam.position.x, y: beam.position.y - 1), beam_type: BeamType.Up);
        case BeamType.Up:
          return (position: (x: beam.position.x + 1, y: beam.position.y), beam_type: BeamType.Right);
        case BeamType.Down:
          return (position: (x: beam.position.x - 1, y: beam.position.y), beam_type: BeamType.Left);  
      }
    //  \
    case MirrorType.LeanLeft:
      switch (beam.beam_type) {
        case BeamType.Left:
          return (position: (x: beam.position.x, y: beam.position.y - 1), beam_type: BeamType.Up);
        case BeamType.Right:
          return (position: (x: beam.position.x, y: beam.position.y + 1), beam_type: BeamType.Down);
        case BeamType.Up:
          return (position: (x: beam.position.x - 1, y: beam.position.y), beam_type: BeamType.Left);  
        case BeamType.Down:
          return (position: (x: beam.position.x + 1, y: beam.position.y), beam_type: BeamType.Right);
      }
  }
}

(Beam, Beam?) split(SplitterType splitter_type, Beam beam) {
  switch(splitter_type) {
    // |
    case SplitterType.Vertical:
      if (beam.beam_type == BeamType.Left || beam.beam_type == BeamType.Right) {
        return ( (position: (x: beam.position.x, y: beam.position.y - 1), beam_type: BeamType.Up), (position: (x: beam.position.x, y: beam.position.y + 1), beam_type: BeamType.Down));
      } else {
        return (advance(beam), null);
      }
    // -
    case SplitterType.Horizontal:
      if (beam.beam_type == BeamType.Up || beam.beam_type == BeamType.Down) {
        return ( (position: (x: beam.position.x - 1, y: beam.position.y), beam_type: BeamType.Left), (position: (x: beam.position.x + 1, y: beam.position.y), beam_type: BeamType.Right));
      } else {
        return (advance(beam), null);
      }
  }
}

int travel(Beam start_beam, Board board) {
  Set<Beam> past_beams = new Set();
  List<Beam> current_beams = [];
  Set<Coordinate> energised = new Set();
  current_beams.add(start_beam);
  past_beams.add(start_beam);
  while (!current_beams.isEmpty) {
    final beam = current_beams.removeLast();
    if (board.outOfBounds(beam.position)) {
      continue;
    }
    energised.add(beam.position);
    if (board.splitters.containsKey(beam.position)) {
      final splitter = board.splitters[beam.position]!;
      final (new_beam, possible_new_beam) = split(splitter.splitter_type, beam);
      if (!past_beams.contains(new_beam)) {
        current_beams.add(new_beam);
        past_beams.add(new_beam);
      }
      if (possible_new_beam != null) {
        if(!past_beams.contains(possible_new_beam!)) {
          current_beams.add(possible_new_beam!);
          past_beams.add(possible_new_beam!);
        }
      }
      
    } else if (board.mirrors.containsKey(beam.position)) {
      final mirror = board.mirrors[beam.position]!;
      final new_beam = reflect(mirror.mirror_type, beam);
      if (!past_beams.contains(new_beam)) {
        current_beams.add(new_beam);
        past_beams.add(new_beam);
      }
    } else {
      Beam new_beam = advance(beam);
      if (!past_beams.contains(new_beam)) {
        current_beams.add(new_beam);
        past_beams.add(new_beam);
      }
    }
  }
  return energised.length;
}

void main(List<String> arguments) {
  if (arguments.length < 1) {
    print ('Please provide an input file :(');
    exit(1);
  }
  final filename = arguments[0];
  File(filename).readAsString()
    .then((String contents) {
      Board board = new Board();
      final lines = contents.split('\n');
      for (var y = 0; y < lines.length; ++y) {
        final line = lines[y];
        for (var x = 0; x < line.length; ++x) {
          Coordinate at_pos = (x: x, y: y);
          switch (line[x]) {
            case '|':
              board.addSplitter(SplitterType.Vertical, at_pos);
            case '-':
              board.addSplitter(SplitterType.Horizontal, at_pos);
            case '/':
              board.addMirror(MirrorType.LeanRight, at_pos);
            case '\\':
              board.addMirror(MirrorType.LeanLeft, at_pos);
          }
        }
      }
      // Part one

      final Beam start_beam = (position: (x: 0, y: 0), beam_type: BeamType.Right);
      final part_one = travel(start_beam, board);
      print("${part_one}");

      // Part two
      var best_value = part_one; // top-left going right
      // Top row going down
      for (var x = 0; x < board.width; ++x) {
        var top_row_going_down = travel((position: (x: x, y: 0), beam_type: BeamType.Down), board);
        if (top_row_going_down > best_value) {
          best_value = top_row_going_down;
        }
      }
      // Left column going right
      for (var y = 1; y < board.height; ++y) { /// can skip corner
        var left_column_going_right = travel((position: (x: 0, y: y), beam_type: BeamType.Right), board);
        if (left_column_going_right > best_value) {
          best_value = left_column_going_right;
        }
      }
      // Bottom row going up
      for (var x = 0; x < board.width; ++x) {
        var bottom_row_going_up = travel((position: (x: x, y: board.height - 1), beam_type: BeamType.Up), board);
        if (bottom_row_going_up > best_value) {
          best_value = bottom_row_going_up;
        }
      }
      // Right column going left
      for (var y = 0; y < board.height; ++y) { /// can skip corner
        var right_column_going_left = travel((position: (x: board.width - 1, y: y), beam_type: BeamType.Left), board);
        if (right_column_going_left > best_value) {
          best_value = right_column_going_left;
        }
      }
      print("${best_value}");
    });
  
}
