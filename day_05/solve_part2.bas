Dim line_txt As String
Dim seeds_start(10240) As _Integer64
Dim seeds_range(10240) As _Integer64

Dim new_seeds_start(10240) As _Integer64
Dim new_seeds_range(10240) As _Integer64


Dim dst_range_start(10240) As _Integer64
Dim src_range_start(10240) As _Integer64
Dim range_lens(10240) As Long


Open "input.txt" For Input As #1
Input #1, line_txt
idx = first_ch_in_str(line_txt, ":")
line_txt = _Trim$(Mid$(line_txt, idx + 1))

num_seed_ranges = 1
seed_read_idx = 0
'Read seed ranges
While idx <> -1
    idx = first_ch_in_str(line_txt, " ")
    number_str$ = ""
    If idx <> -1 Then
        number_str$ = _Trim$(Mid$(line_txt, 1, idx))
        line_txt = _Trim$(Mid$(line_txt, idx + 1))
    Else
        number_str$ = line_txt
    End If
    number&& = Val(number_str$)
    seed_read_idx = seed_read_idx + 1
    If seed_read_idx Mod 2 = 1 Then
        If seed_read_idx > 1 Then
            num_seed_ranges = num_seed_ranges + 1
        End If
        seeds_start(num_seed_ranges) = number&&
    Else
        seeds_range(num_seed_ranges) = number&&
    End If
    seeds(num_seeds) = number&&
Wend

'ignore whitespace line
Input #1, line_txt
Cls
While Not EOF(1)
    'ignore line with mapping type (assume they're in a decent order)
    Input #1, line_txt
    line_txt = _Trim$(line_txt)
    num_mappings = 0

    While Len(line_txt) > 0 And Not EOF(1)
        Input #1, line_txt
        line_txt = _Trim$(line_txt)
        If Len(line_txt) > 0 Then
            num_mappings = num_mappings + 1
            idx = first_ch_in_str(line_txt, " ")
            dst_range$ = _Trim$(Mid$(line_txt, 1, idx - 1))
            idx_prev = idx
            idx = first_ch_in_str(Mid$(line_txt, idx + 1), " ")
            src_range$ = _Trim$(Mid$(line_txt, idx_prev + 1, idx - 1))
            range_len$ = _Trim$(Mid$(line_txt, idx_prev + idx + 1))
            dst_range_start(num_mappings) = Val(dst_range$)
            src_range_start(num_mappings) = Val(src_range$)
            range_lens(num_mappings) = Val(range_len$)
        End If
    Wend
    i = 1
    new_num_ranges = 0
    While i <= num_seed_ranges
        range_start&& = seeds_start(i)
        range_end&& = seeds_start(i) + seeds_range(i) - 1
        i = i + 1
        found = 0
        For j = 1 To num_mappings
            src_start&& = src_range_start(j)
            src_end&& = range_lens(j) + src_start&& - 1
            If range_start&& > src_end&& Or range_end&& < src_start&& Then
                'continue
            Else
                'theres an intersection
                'seed range starts before the src_range, so add whatever is before that to the end of the seed ranges
                If range_start&& < src_start&& Then
                    num_seed_ranges = num_seed_ranges + 1
                    seeds_start(num_seed_ranges) = range_start&&
                    seeds_range(num_seed_ranges) = src_start&& - range_start&&
                End If
                intersection_start&& = range_start&&
                If src_start&& > intersection_start&& Then
                    intersection_start&& = src_start&&
                End If
                'seed range extends beyond the src_range, so add the rest back to the list
                If range_end&& > src_end&& Then
                    num_seed_ranges = num_seed_ranges + 1
                    seeds_start(num_seed_ranges) = src_end&& + 1
                    seeds_range(num_seed_ranges) = range_end&& - src_end&&
                End If
                intersection_end&& = range_end&&
                If src_end&& < intersection_end&& Then
                    intersection_end&& = src_end&&
                End If
                new_num_ranges = new_num_ranges + 1
                new_seeds_start(new_num_ranges) = intersection_start&& - src_start&& + dst_range_start(j)
                new_seeds_range(new_num_ranges) = intersection_end&& - intersection_start&& + 1
                found = 1
                Exit For
            End If
        Next
        If found = 0 Then
            new_num_ranges = new_num_ranges + 1
            new_seeds_start(new_num_ranges) = range_start&&
            new_seeds_range(new_num_ranges) = range_end&& - range_start&& + 1
        End If
    Wend
    For j = 1 To new_num_ranges
        seeds_start(j) = new_seeds_start(j)
        seeds_range(j) = new_seeds_range(j)
    Next
    num_seed_ranges = new_num_ranges


Wend
Close #1
min_seed&& = seeds_start(1)

For i = 1 To num_seed_ranges
    If seeds_start(i) < min_seed&& Then
        min_seed&& = seeds_start(i)
    End If
Next

Print Str$(min_seed&&)

Function first_ch_in_str (str_val As String, ch As String)
    first_ch_in_str = -1
    For idx = 0 To Len(str_val) - 1
        If Mid$(str_val, idx, 1) = ch Then
            first_ch_in_str = idx
            Exit For
        End If
    Next
End Function

