# Advent of Code 2023

[Advent of Code](https://adventofcode.com/) is a lovely programming challenge that takes place in the month of December, leading up to Christmas. I've started doing the challenge since 2019 and used it as an opportunity to sharpen my Rust knowledge (and initially to learn Swift as well).

This year, I'm taking it to a new extreme. I'm doing it in 26 languages, 1 per day (with one day being 'double booked'). Since problems increase in difficulty, I will not go in alphabetical order; the order is **not** set in stone, but this is the list:

- [Ada](https://ada-lang.io/)
    - Specifically, I'll be focusing on Ada, not SPARK.
- [Bash](https://www.gnu.org/software/bash)
    - I may end up using ZSH to run the program, but zsh-exclusive features are off-limits.
- [Clojure](https://clojure.org/)
- [Dart](https://dart.dev/)
    - No Flutter tho.
- [Elixir](https://elixir-lang.org/)
- [Falcon](http://www.falconpl.org/)
- [Golang](https://go.dev/)
    - The official version, not any of the smøller variants.
- [Haskell](https://www.haskell.org/)
    - The GHC version, not HUGS.
- [Io](https://iolanguage.org/)
- [Julia](https://julialang.org/)
    - Probably not using a Jupyter (or is it Pluto?) notebook.
- [Kotlin](https://kotlinlang.org/)
    - In IntelliJ IDEA CE.
- [Lua](https://www.lua.org/)
    - Regular version, no specific bells and whistles.
- [MatLab](https://octave.org/)
    - Yes, it's Octave, not MatLab. Go cry about it.
- [Nim](https://nim-lang.org/)
- [Objective-C](https://developer.apple.com/library/archive/documentation/Cocoa/Conceptual/ProgrammingWithObjectiveC/Introduction/Introduction.html)
    - Just using XCode.
- [Python](https://www.python.org/)
    - Probably whatever Python 3 is available on my machine by default.
- [QBasic](https://qb64.com/)
    - Yes, Quick Basic counts as Q instead of B. Again, go cry about it.
- [Rust](https://www.rust-lang.org/)
    - My favourite before trying all of them
- [Swift](https://www.swift.org/)
    - I will likely use XCode for this as well, although I suppose there's a regular open source version I could use instead. Haven't decided yet.
- [TypeScript](https://www.typescriptlang.org/)
    - Using [Bun](https://oven.sh/)
- [Ursa lang](https://ursalang.github.io/)
    - This is the least established programming language; will be rough.
- [V](https://vlang.io/)
- [Whiley](https://whiley.org/)
    - Nearly chose [Wisp](https://www.draketo.de/software/wisp), but I couldn't find proper docs on how to use it
- [Xtend](https://eclipse.dev/Xtext/xtend/)
    - Using Eclipse; I believe Java is proper Xtend, so :shrug:
- [Yorick](https://yorick.sourceforge.net/)
    - Looks like an excuse to use Octave twice, but there are no good Y alternatives.
- [Zig](https://ziglang.org/)
    - This looks like an awful way to learn Zig, but :shrug:

The tentative order is:

1. Bash
1. Yorick
1. MatLab
1. Io
1. QBasic
1. Falcon
1. Whiley
1. Julia
1. Clojure and Elixir double billing
1. Lua
1. Haskell
1. Ada
1. V
1. Zig
1. Nim
1. Dart
1. Xtend
1. Kotlin
1. Golang
1. Python
1. Typescript
1. Objective-C
1. Swift
1. Rust
1. Ursalang
