#!/usr/bin/env io

f := File openForReading("input.txt")

totalPoints := 0
cards_won_for_card_id := Map clone
all_card_ids := List clone

while(line := f readLine,
    id_cards := line split(":")
    id := id_cards at(0)
    id = id splitNoEmpties(" ") at(1) asNumber
    all_card_ids append(id asString)
    cards := id_cards at(1)
    cards = cards split("|")
    winning_cards := cards at(0) strip splitNoEmpties(" ")
    have_cards := cards at(1) strip splitNoEmpties(" ")
    common_cards := winning_cards intersect(have_cards)
    if (common_cards size > 0,
        totalPoints = totalPoints + (1 shiftLeft (common_cards size - 1))
        cards_won_for_card_id atPut(id asString, common_cards size)
    )
)

totalPoints println

total_cards_won := 0
cards_won := Map clone
all_card_ids foreach(k, 
    cards_won atPut(k, 1)
)
loop(
    new_cards_won := Map clone
    cards_won foreach(card_id, number_of_cards,
        total_cards_won = total_cards_won + number_of_cards
        if (cards_won_for_card_id hasKey(card_id),
            new_tickets_to_add := cards_won_for_card_id at(card_id)
            first_id := card_id asNumber
            last_id := first_id + new_tickets_to_add
            first_id = first_id + 1
            for (new_id, first_id, last_id,
                if (new_cards_won hasKey(new_id asString),
                    old_value := new_cards_won at(new_id asString)
                    new_value := old_value + number_of_cards
                    new_cards_won atPut(new_id asString, new_value),
                    new_cards_won atPut(new_id asString, number_of_cards)
                )
            )
        )    
        
    )
    if (new_cards_won isEmpty, 
        break,
        cards_won = new_cards_won clone)
)

total_cards_won println
