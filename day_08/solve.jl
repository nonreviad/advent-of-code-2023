f = open("input.txt", "r")

input_str::String = read(f, String)

splits = split(input_str, "\n\n")
instruction = splits[1]
mappings_as_str = splits[2]

global mappings = Dict{String, Tuple{String, String}}()

for mapping_line in split(mappings_as_str, '\n')
   split_line = split(mapping_line, " = ")
   node = split_line[1]
   children = split(chop(split_line[2], head=1, tail=1), ", ")
   left = children[1]
   right = children[2]
   mappings[node] = (left, right)
end

num_steps::Int64 = 0
instr_index::UInt32 = 1

function steps_to_end_state(start_state, mappings, is_end_state, instructions)
    num_steps = 0
    instr_index = 1
    state = start_state
    while !is_end_state(state)
        direction = instruction[instr_index]
        num_steps += 1
        instr_index += 1
        if instr_index > length(instruction)
            instr_index = 1
        end
        if direction == 'L'
           state = mappings[state][1]
        elseif direction == 'R'
           state = mappings[state][2]
        end
    end
    return num_steps
end

println(steps_to_end_state("AAA", mappings, s -> s == "ZZZ", instruction))

steps_per_state::Vector{Int64} = Vector{Int64}()
for state in keys(mappings)
    if state[3] == 'A'
        local num_steps = steps_to_end_state(state, mappings, s -> s[3] == 'Z', instruction)
        push!(steps_per_state, num_steps)
    end
end

println(lcm(steps_per_state))


