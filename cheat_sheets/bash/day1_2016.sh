#!/bin/bash

directions=("NORTH" "EAST" "SOUTH" "WEST")
direction_idx=0

declare -a visited=("0 0")

first="NONE"
x=0
y=0

for i in $(cat $1 | tr ", " "\n"); do
    turn_direction=${i:0:1}
    distance=${i:1}
    case $turn_direction in
    R)
        direction_idx=$((direction_idx+1))
        direction_idx=$((direction_idx%4))
    ;;
    L)
        direction_idx=$((direction_idx+3))
        direction_idx=$((direction_idx%4))
    ;;
    esac
    case ${directions[direction_idx]} in
    NORTH)
        x_begin=$x
        x_final=$x
        y_begin=$((y+1))
        y_final=$((y+distance))
    ;;
    EAST)
        x_begin=$((x+1))
        x_final=$((x+distance))
        y_begin=$y
        y_final=$y
    ;;
    SOUTH)
        x_begin=$x
        x_final=$x
        y_begin=$((y-1))
        y_final=$((y-distance))
    ;;
    WEST)
        x_begin=$((x-1))
        x_final=$((x-distance))
        y_begin=$y
        y_final=$y
    ;;
    esac

    if [ "$first" = "NONE" ]; then
        for xx in $(seq $x_begin $x_final); do
            for yy in $(seq $y_begin $y_final); do
            pos="$xx $yy"
            for i in "${visited[@]}"; do
                if [ "$pos" = "$i" ]; then
                    if [ "$first" = "NONE" ]; then
                        first=$i
                    fi
                fi
            done
            visited+=("$pos")
            done
        done
    fi
    x=$x_final
    y=$y_final
done

manhattan_distance() {
    local pos=$1
    local distance=0
    for v in $(echo $pos | tr " " "\n"); do
        if [ $v -lt 0 ]; then
            distance=$((distance-v))
        else
            distance=$((distance+v))
        fi
    done
    echo $distance
}

final_distance=$(manhattan_distance "$x $y")
echo $final_distance
distance_to_first_doubly_visited=$(manhattan_distance "$first")
echo $distance_to_first_doubly_visited