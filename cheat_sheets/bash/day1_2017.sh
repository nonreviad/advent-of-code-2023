#!/bin/zsh

# Sol'n for Day 1 2017

input_data=$(cat $1)
input_len=${#input_data}

sum=0
for i in $(seq 1 $input_len); do
    digit=$input_data[$i]
    next_idx=$((i+1))
    while [ $next_idx -gt $input_len ]; do
        next_idx=$((next_idx-input_len))
    done
    next_digit=$input_data[$next_idx]
    if [ $digit -eq $next_digit ]; then
        sum=$((sum+digit))
    fi
done

echo $sum


sum=0
delta=$((input_len/2))
for i in $(seq 1 $input_len); do
    digit=$input_data[$i]
    next_idx=$((i+delta))
    while [ $next_idx -gt $input_len ]; do
        next_idx=$((next_idx-input_len))
    done
    next_digit=$input_data[$next_idx]
    if [ $digit -eq $next_digit ]; then
        sum=$((sum+digit))
    fi
done

echo $sum