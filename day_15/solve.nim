import strutils
import std/strformat
func hashme(s: string): int =
    var val = 0
    for ch in s:
        val = ((val + ord(ch)) * 17) mod 256
    return val
const data = slurp("input.txt")
var sum = 0
for instruction in data.split(","):
    sum += hashme(instruction)

echo fmt"{sum}"


type
    Lens = tuple
        name: string
        focal_length: int
type
    Entry = seq[Lens]
    HashMap = array[0..255, Entry]



func indexOf(entry: Entry, name: string): int =
    for idx, val in entry[0 .. ^1]:
        if val.name == name:
            return idx
    return -1

proc setVal(hash_map: var HashMap, name: string, focal_length: int): void = 
    let hash_val = hashme(name)
    let idx = indexOf(hash_map[hash_val], name)
    if idx == -1:
        hash_map[hash_val].add((name: name, focal_length: focal_length))
    else:
        hash_map[hash_val][idx] = (name: name, focal_length: focal_length)

proc rmVal(hash_map: var HashMap, name: string): void =
    let hash_val = hashme(name)
    let idx = indexOf(hash_map[hash_val], name)
    if idx != -1:
        hash_map[hash_val].delete(Natural(idx))

var hm: HashMap
for i in countup(0, 255):
    hm[i] = @[]

for instruction in data.split(","):
    if instruction.contains("="):
        let sv = instruction.split("=")
        let s = sv[0]
        let v = parseInt(sv[1])
        setVal(hm, s, v)
    else:
        let sv = instruction.split("-")
        let s = sv[0]
        rmVal(hm, s)

var focusing_power = 0

for box_idx, box in hm[0 .. ^1]:
    for slot_idx, slot in box[0 .. ^1]:
        focusing_power += (box_idx + 1) * (slot_idx + 1) * slot.focal_length

echo fmt"{focusing_power}"