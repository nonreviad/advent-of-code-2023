#!/bin/zsh

input_file=$1
sum=0
for sanitized_line in $(cat $input_file | sed 's/[a-z]//g'); do
    sz=${#sanitized_line}
    num=0
    first_digit=$sanitized_line[1]
    last_digit=$sanitized_line[$sz]
    num=$((first_digit*10))
    num=$((num+last_digit))
    sum=$((sum+num))
done
echo $sum

sum=0

name_to_digit() {
    case $1 in
        "one")   echo "1" ;;
        "two")   echo "2" ;;
        "three") echo "3" ;;
        "four")  echo "4" ;;
        "five")  echo "5" ;;
        "six")   echo "6" ;;
        "seven") echo "7" ;;
        "eight") echo "8" ;;
        "nine")  echo "9" ;;
    esac
}

remove_letters() {
    echo "$1" | sed 's/[a-z]//g'
}

replace_all_spelled_out_digits() {
    local input_str=$1
    len=${#input_str}
    if [ $len -lt 3 ]; then
        input_str=$(remove_letters $input_str)
        echo $input_str
    else
        up_to_first_spelled_out_digit=$(echo $input_str | sed -E "s/(one|two|three|four|five|six|seven|eight|nine).*$//")
        if [ ${#up_to_first_spelled_out_digit} -lt $len ]; then
            before_len=${#up_to_first_spelled_out_digit}
            before=$up_to_first_spelled_out_digit
            before=$(remove_letters $before)
            after=${input_str:$((before_len))}
            digit=$(echo $after | egrep -o "^(one|two|three|four|five|six|seven|eight|nine)")
            digit_name_len=${#digit}
            after=${after:$((digit_name_len))}
            after=$(replace_all_spelled_out_digits $after)
            digit=$(name_to_digit $digit)
            echo "${before}${digit}${after}"
        else
            input_str=$(remove_letters $input_str)
            echo $input_str
        fi
    fi
}
sum=0

line_num=0
for line in $(cat $input_file); do
    line_num=$((line_num+1))
    sanitized_line=$(replace_all_spelled_out_digits $line)
    num=0
    sz=${#sanitized_line}
    first_digit=$sanitized_line[1]
    last_digit=$sanitized_line[$sz]
    num=$((first_digit*10))
    num=$((num+last_digit))
    sum=$((sum+num))
done

echo $sum