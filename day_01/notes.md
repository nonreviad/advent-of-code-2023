Problem was lovely, Bash was not.

`sed` carried.

The first part was easy enough, only had to process line by line, find the digits, and take the first and last. Either `sed` or probably `tr` could probably achieve the same result.

The second part was far more tricky. I tried to be clever and just set the order of the matches, but that doesn't work.

Basically did a greedy search and replace iteratively to do the same sanitisation. It sucks.

