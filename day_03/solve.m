f = fopen("input.txt", "r");
s = 0;
line_id=0;
num_lines=140
num_cols=140
input_mat=zeros(num_lines, num_cols);
numbers_mat=zeros(num_lines, num_cols);

while true
    line = fgetl(f);
    if ~ischar(line)
        break;
    end
    line_id += 1;
    col_id=0;
    last_num=0;
    for ch=typecast(line, "uint8")
        col_id += 1;
        big_ch=typecast([ch, 0, 0, 0, 0, 0, 0, 0], "uint64");
        input_mat(line_id, col_id) = big_ch;
    end
end
for line = 1:num_lines
    start_col = -1;
    for col = 1:num_cols
        val = input_mat(line, col);
        if is_digit(val) && start_col == -1
            start_col = col;
        end
        if start_col != -1 && (!is_digit(val) || col==num_cols)
            end_col=col;
            if !is_digit(val)
                end_col-=1;
            end
            has_symbol=false;
            for other_line=(line-1):(line+1)
            for other_col=(start_col-1):(end_col+1)
                if other_line > 0 && other_col > 0 && other_line <= num_lines && other_col <= num_cols && is_symbol(input_mat(other_line, other_col))
                    has_symbol = true;
                end
            end
            end
            num = 0;
            for c=start_col:end_col
                num = num*10 + input_mat(line,c) - 48;
            end
            if has_symbol
                s += num;
            end
            numbers_mat(line, start_col:end_col) = num;
            start_col=-1;
        end
    end
end

printf("%d\n", s);

other_s = 0;

for line=1:num_lines
    for col=1:num_cols
        % found a *
        if input_mat(line, col) == 42
            p = 1;
            number_of_numbers = 0;
            for other_line=(line-1):(line+1)
                for other_col=(col-1):(col+1)
                    if other_line > 0 && other_line <= num_lines && other_col > 0 && other_col <= num_cols && numbers_mat(other_line, other_col) != 0
                        to_the_left = other_col - 1;
                        if to_the_left < 1 || to_the_left == (col-2) || numbers_mat(other_line, to_the_left) == 0
                            number_of_numbers += 1;
                            p *= numbers_mat(other_line, other_col);
                            
                        end
                    end
                end
            end
            if number_of_numbers == 2
                other_s += p;
            end
        end
    end
end

printf("%d\n", other_s);